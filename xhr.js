const getBtn = document.getElementById("get-btn");
const postBtn = document.getElementById("post-btn");


const sendHttpRequest = (method,url,data) => {
const promise = new Promise ((resolve,reject)=>{
    const xhr = new XMLHttpRequest();

    //open function prepare the http req 
    xhr.open(method, url);

    xhr.responseType = 'json'

    if(data){
    xhr.setRequestHeader('Content-Type','application/json')
    }

    xhr.onload = () => {
        //This will be only one long string in response which contains JSON data

        try {
            console.log("Data::" + xhr.response)
            resolve(xhr.response);
        } catch (e) {
            console.log("Error:" + e)
        }

    }

    xhr.onerror = () => {
        reject("Oops something went wrong!")
    };

    //This will send the pre-configured request 
    xhr.send(JSON.stringify(data));


});

return promise;
    
}

function getData() {
    console.log("in getData method:")
    sendHttpRequest("GET",'https://reqres.in/api/users').then((responseData) => {
        console.log(responseData);
    })
    .catch( errorMsg => {
        console.log(errorMsg);
    } )
   
};

function sendData() {
    sendHttpRequest("POST",'https://reqres.in/api/register',
        {
        "email": "eve.holt@reqres.in"
        }
    ).then( (responseData) => {
        console.log(responseData);
    })
    .catch((errorMsg) => {
        console.log(errorMsg);
    })
};


getBtn.addEventListener("click", getData);
postBtn.addEventListener("click", sendData);
