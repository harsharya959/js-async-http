const getBtn = document.getElementById("get-btn");
const postBtn = document.getElementById("post-btn");

//unlike xmlHttprequest fetch is already wrap up in Promise and it returns a promise
const sendFetchRequest = (method,url,data) => {

   return fetch(url, {
       method: method,
       body: JSON.stringify(data),
       headers: data ? { 'Content-Type': 'application/json'} : {}
   })
    .then(response => {
        console.log(response);
        if(response.status >=400){
          console.log("Something went wrong")
        }
        //this will return a promise 
        return response.json();
    })

}

const getData = () => {
    sendFetchRequest('GET', 'https://reqres.in/api/users')
    .then( responseData => {
        console.log(responseData);
    })
    .catch( err = {
    
    }); 

};

const sendData = () => {
   sendFetchRequest('POST', 'https://reqres.in/api/register',
    {
        email : "eve.holt@reqres.in",
        password : "pistol"
    }
   ).then( responseData => {
    console.log(responseData);
   });
};


getBtn.addEventListener("click", getData);
postBtn.addEventListener("click", sendData);
